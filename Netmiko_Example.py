#! /bin/usr/python

from netmiko import ConnectHandler
from creds import routers
from prettyprinter import pprint


# Get Prompt for the first router (R2) - test connectivity
def demo1():
    with ConnectHandler(**routers["r2"]) as handle:
        print(handle.find_prompt())


## Get Prompts for all devices - test for connectivity
def demo2():
    for router in routers.values():
        with ConnectHandler(**router) as handle:
            print(handle.find_prompt())


# Show commands
def demo3():
    command = "show ip int brief"
    for router in routers.values():
        with ConnectHandler(**router) as handle:
            output = handle.send_command(command)
            print(f'show version for {router["host"]}')
            print("~" * 30)
            print("\n")
            pprint(output)


# Show commands us textFSM library (structured output)
def demo4():
    command = "show ip int brief"
    for router in routers.values():
        with ConnectHandler(**router) as handle:
            output = handle.send_command(command, use_textfsm=True)
            print(f'show ip int brief for {router["host"]}')
            print("~" * 30)
            print("\n")
            pprint(output)


# sending configurations as a list of commands
def demo5():
    commands = [
        "service timestamps debug datetime msec",
        "service timestamps log datetime msec",
        "service password-encryption",
    ]
    for router in routers.values():
        with ConnectHandler(**router) as handle:
            output = handle.send_config_set(commands)
            output += handle.save_config()
            print(output)


# sending configurations from a file
def demo6():
    config_file = "netmikoconfig.txt"
    for router in routers.values():
        with ConnectHandler(**router) as handle:
            output = handle.send_config_from_file(config_file)
            output += handle.save_config()
            print(output)


def main():
    demo1()
    # demo2()
    # demo3()
    # demo4()
    # demo5()
    # demo6()


if __name__ == "__main__":
    main()
