#! /usr/bin/python

from nornir import InitNornir
from nornir_jinja2.plugins.tasks import template_file
from nornir_utils.plugins.functions import print_result
from nornir_napalm.plugins.tasks import napalm_get
from nornir_netmiko.tasks import netmiko_send_config, netmiko_send_command
from nornir.core.task import Task, Result
import textfsm

# defines nornir object as nr and set configuration variables
nr = InitNornir("config.yml")


def showInventory():
    # prints host inventory
    print(f"\n The hosts file shows {nr.inventory.hosts} hosts\n")
    # prints a single host
    print(f' the hostname of R2 is {nr.inventory.hosts["r2"]}\n')
    # print host keys. notice the inheritence from the groups.yml
    print(f' R2 has the following keys {nr.inventory.hosts["r2"].keys()}\n')
    # print value for a key
    print(
        f' R2 has a description field that was inherrited from the group, which is {nr.inventory.hosts["r2"]["description"]}\n'
    )
    # we can filter reports by attributes as well

    siteFilter = nr.filter(loopbackIP="10.200.254.2").inventory.hosts.items()
    print(siteFilter)

    siteFilter = nr.filter(loopbackIP="10.200.254.3").inventory.hosts.items()
    print(siteFilter)

    siteFilter = nr.filter(type="edge").inventory.hosts.items()
    print(siteFilter)

    siteFilter = nr.filter(type="core").inventory.hosts.items()
    print(siteFilter)


def previewTemplate(task):
    task.run(
        task=template_file,
        name=f"creating template for router {task.host}",
        template="nornir_config.j2",
        path="./",
        description=f"loopback interface for router {task.host}",
    )


def createTemplateFiles(task):
    template = task.run(
        task=template_file,
        name=f"creating template for router {task.host}",
        template="nornir_config.j2",
        path="./",
        description=f"loopback interface for router {task.host}",
    )
    # print(template[0])

    with open(f"{task.host}_Template.txt", "w") as file:
        file.write(str(template[0]))


def pushConfigToRouters(task):
    pushConfig = task.run(
        task=netmiko_send_config, config_file=f"{task.host}_Template.txt"
    )


def validation(task):
    task.run(
        task=napalm_get,
        name=f"{task.host} Napalm get interfaces",
        getters=["get_interfaces_ip"],
    )
    output= task.run(
        task=netmiko_send_command,
        name=f"{task.host} netmiko - Show ip route",
        command_string="show ip route",
    )
    inputText=str(output[0])
    task.run(
        task=textFSMify,
        inputText = inputText,
        fsmTemplate = 'cisco_ios_show_ip_route.textfsm',
        )
    output = task.run(
        task=netmiko_send_command,
        name=f"{task.host} netmiko - show ip eigrp neihbor",
        command_string="sh ip eigrp neighbor",
    )
    inputText=str(output[0])
    task.run(
        task=textFSMify,
        inputText = inputText,
        fsmTemplate = 'cisco_ios_show_ip_eigrp_neighbors.textfsm',
        )
def textFSMify(task: Task, inputText,fsmTemplate) ->Result:
    template_path = '/Users/jonathangreenberg/Dropbox/Programming/Python/ntc-templates/templates/'
    template = template_path+fsmTemplate
    with open(template) as f:
        fsm = textfsm.TextFSM(f)
        result = fsm.header, fsm.ParseText(inputText)
    
    return Result(host=task.host, result=result)


def main():
    # showInventory()
    template = nr.run(task=previewTemplate)
    print_result(template)
    # print(task['r2'][1])
    userInput = str.lower(
        input(
            "\n does this output look correct? Do you want to push it to the routers? "
        )
    )
    if userInput == "y" or userInput == "yes":
        nr.run(task=createTemplateFiles)
        pushConfig = nr.run(task=pushConfigToRouters)
        print_result(pushConfig)
        userInput = input("\n press any key to see the interfaces and routing table")
        result = nr.run(task=validation)
        print_result(result)

    elif userInput == "n" or userInput == "no":
        print("OK. Go back and fix your config file and Jinja Template. Exiting")
        exit()
    else:
        print("Invalid input. Exiting")
        exit()


if __name__ == "__main__":
    main()
