#! /bin/usr/python

# Imports Packages and Modules
from jinja2 import Template, Environment, FileSystemLoader


# Simple demo function to demonstrate simple use case of Template
def demo1():
    template1 = Template("Hello {{variable}}!!")
    output = template1.render(variable="UDT")
    print(output)
    return ()


# using a Jinja2 (j2) file and a dictionary for config templating
def demo2():
    switchDict = {
        "hostname": "My Switch",
        "IP": "192.168.1.1",
        "mask": "255.255.255.0",
        "secret": "cisco",
        "domain": "abc.com",
        "vtpdomain": "novtp",
        "vtppass": "novtp",
        "voipVLAN": "20",
        "voipVLANName": "voice VLAN",
        "accessDescription": "!!!PC and Phones!!!",
        "trunkDescription": "uplinks to other switches",
        "DG": "192.168.1.1",
        "NTPIP": "192.168.1.254",
    }

    j2_env = Environment(
        loader=FileSystemLoader("./"), trim_blocks=True, autoescape=True
    )
    output = j2_env.get_template("config.j2").render(dictitem=switchDict)
    print(output)
    return ()


# configure multiple switches using a dictionary
def demo3():
    switches = {
        "r1": {"hostname": "R1", "IP": "192.168.1.1"},
        "r2": {"hostname": "R2", "IP": "192.168.1.2}"},
    }
    for switch in switches.values():
        switchDict = {
            "hostname": switch["hostname"],
            "IP": switch["IP"],
            "mask": "255.255.255.0",
            "secret": "cisco",
            "domain": "abc.com",
            "vtpdomain": "novtp",
            "vtppass": "novtp",
            "voipVLAN": "20",
            "voipVLANName": "voice VLAN",
            "accessDescription": "!!!PC and Phones!!!",
            "trunkDescription": "!!!uplinks to other switches!!!",
            "DG": "192.168.1.1",
            "NTPIP": "192.168.1.254",
        }
        j2_env = Environment(
            loader=FileSystemLoader("./"), trim_blocks=True, autoescape=True
        )
        output = j2_env.get_template("config.j2").render(dictitem=switchDict)
        print(output)
    return ()


# Same example as demo3 but writes each configuration to a file based on the the switch hostname
def demo4():
    switches = {
        "r1": {"hostname": "R1", "IP": "192.168.1.1"},
        "r2": {"hostname": "R2", "IP": "192.168.1.2"},
    }
    for switch in switches.values():
        switchDict = {
            "hostname": switch["hostname"],
            "IP": switch["IP"],
            "mask": "255.255.255.0",
            "secret": "cisco",
            "domain": "abc.com",
            "vtpdomain": "novtp",
            "vtppass": "novtp",
            "voipVLAN": "20",
            "voipVLANName": "voice VLAN",
            "accessDescription": "!!!PC and Phones!!!",
            "trunkDescription": "!!!uplinks to other switches!!!",
            "DG": "192.168.1.1",
            "NTPIP": "192.168.1.254",
        }
        j2_env = Environment(
            loader=FileSystemLoader("./"), trim_blocks=True, autoescape=True
        )
        output = j2_env.get_template("config.j2").render(dictitem=switchDict)
        with open(f"{switch['hostname']}.txt", "w") as f:
            f.write(output)
    return ()


# main function
def main():
    demo1()
    # demo2()
    # demo3()
    # demo4()


# good practice to include in all applications
if __name__ == "__main__":
    main()
