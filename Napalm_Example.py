#! /usr/bin/python
import creds
from napalm import get_network_driver
from prettyprinter import pprint

# addtional getters index https://napalm.readthedocs.io/en/latest/support/index.html#getters-support-matrix
# get facts about device
def demo1(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        print(host, "\n", "~" * 20)
        pprint(device.get_facts())


# get interface count
def demo2(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        ifaceCount = device.get_interfaces_counters()
        pprint(ifaceCount)


# use interface counts to provide error checking
def demo3(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        ifaceCount = device.get_interfaces_counters()
        for ifaceName, ifaceStats in ifaceCount.items():
            print(ifaceName, "~" * 30)
            for statName, statNumber in ifaceStats.items():
                if "error" in statName:
                    if int(statNumber) == 0:
                        print(f"{statName} has errors. Please inspect")
            print("\n")


# send standard cli commands
def demo4(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        pprint(device.cli(["sh ip int brief"]))


# load a configuration for merge (doesn't work in iol devices)
def demo5(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        result = device.load_merge_candidate(filename="netmikoconfig.txt")
        print(result)


# ping devices
def demo6(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        pprint(device.ping(host))


# traceroute
def demo7(host, uname, pword):
    driver = get_network_driver("ios")
    with driver(host, uname, pword) as device:
        troute = device.traceroute("4.2.2.2")
        print(troute)


def main():
    routers = creds.routers.values()
    for router in routers:
        host = router["host"]
        uname = router["username"]
        pword = router["password"]
        # demo1(host,uname,pword)
        # demo2(host,uname,pword)
        # demo3(host,uname,pword)
        # demo4(host,uname,pword)
        # demo5(host,uname,pword)
        # demo6(host,uname,pword)
        # demo7(host,uname,pword)


if __name__ == "__main__":
    main()
